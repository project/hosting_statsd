<?php

/**
 * Implements hook_menu().
 *
 */
function hosting_statsd_menu() {

  $items['admin/config/development/statsd/hosting'] = array(
    'title'            => 'Aegir Stats',
    'description'      => 'Configure what Aegir statistics to send to statsD.',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hosting_statsd_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/development/statsd/settings'] = array(
    'title'            => 'Configure',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );
  return $items;

}

/**
 * Page callback for StatsD administrative settings.
 */
function hosting_statsd_admin_settings() {

  $form['hosting_statsd_hosting_context_counts'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Send Hosting Context Counts'),
    '#description'   => t('Regulary report the number of sites, platforms, servers.'),
    '#default_value' => variable_get('hosting_statsd_hosting_context_counts', TRUE),
  );
  $form['hosting_statsd_hosting_task_update_events'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Send Task Status Events'),
    '#description'   => t('Send an event every time a task status is updated.'),
    '#default_value' => variable_get('hosting_statsd_hosting_task_update_events', TRUE),
  );

  $names = hosting_task_status_name();
  $options = array_combine($names, $names);
  $form['hosting_statsd_hosting_task_update_events_statuses'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Task Status Triggers'),
    '#description'   => t('Select the task statuses that will trigger an event notification. Leave unchecked to fire on all states.'),
    '#default_value' => variable_get('hosting_statsd_hosting_task_update_events_statuses', array()),
    '#options' => $options,
  );
  return system_settings_form($form);
}

/**
 * Implements hook_hosting_task_update_status().
 */
function hosting_statsd_hosting_task_update_status($task, $status)
{
  statsd_call('hosting_tasks.queued', 'gauge', hosting_task_count());
  statsd_call('hosting_tasks.processing', 'gauge', hosting_task_count_running());

  if (variable_get('hosting_statsd_hosting_task_update_events', TRUE) && $status == HOSTING_TASK_ERROR || $status == HOSTING_TASK_SUCCESS || $status == HOSTING_TASK_WARNING) {
    statsd_call('hosting_tasks.' . $task->ref_type . '.' . $task->task_type . '.' . hosting_task_status_name($status), 'timing', $task->delta * 1000);
  }
}

/**
 * Implements hook_cron().
 */
function hosting_statsd_cron()
{

  statsd_call('hosting_tasks.queued', 'gauge', hosting_task_count());
  statsd_call('hosting_tasks.processing', 'gauge', hosting_task_count_running());

  if (variable_get('hosting_statsd_hosting_context_counts', TRUE)) {
    $totals = db_query("SELECT n.type,  COUNT(*) as count FROM {node} n INNER JOIN {hosting_context} c ON n.nid = c.nid WHERE n.status = 1 AND n.type IN (:types) GROUP BY n.type", array(
      ':types' => array('site', 'platform', 'server', 'client')
    ))->fetchAllKeyed();

    foreach ($totals as $type => $total) {
      statsd_call('hosting_context.' . $type, 'gauge', $total);
    }
  }
}